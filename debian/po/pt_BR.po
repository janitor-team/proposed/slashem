# Debconf translations for slashem.
# Copyright (C) 2016 THE slashem'S COPYRIGHT HOLDER
# This file is distributed under the same license as the slashem package.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: slashem 0.0.7E7F3-7\n"
"Report-Msgid-Bugs-To: slashem@packages.debian.org\n"
"POT-Creation-Date: 2011-07-13 19:45-0700\n"
"PO-Revision-Date: 2016-01-07 18:05-0200\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@arg.eti.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../slashem-common.templates:1001 ../slashem-common.templates:2001
msgid "abort"
msgstr "cancelar"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../slashem-common.templates:1001 ../slashem-common.templates:2001
msgid "backup"
msgstr "cópia de segurança"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../slashem-common.templates:1001 ../slashem-common.templates:2001
msgid "purge"
msgstr "expurgar"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../slashem-common.templates:1001 ../slashem-common.templates:2001
msgid "ignore"
msgstr "ignorar"

#. Type: select
#. Description
#. Type: select
#. Description
#: ../slashem-common.templates:1002 ../slashem-common.templates:2002
msgid "Should Slash'em back up your old, incompatible save files?"
msgstr ""
"O Slash'em deve fazer cópia de segurança dos seus antigos e incompatíveis "
"arquivos salvos?"

#. Type: select
#. Description
#. Type: select
#. Description
#: ../slashem-common.templates:1002 ../slashem-common.templates:2002
msgid ""
"You are upgrading from a version of Slashe'em whose save files are not "
"compatible with the version you are upgrading to. You may either have them "
"backed up into /tmp, purge them, ignore this problem completely, or abort "
"this installation and manually handle Slashem's save files."
msgstr ""
"Você está atualizando a partir de uma versão do Slash'em cujos arquivos "
"salvos não são compatíveis com a versão para a qual você está atualizando. "
"Você pode fazer uma cópia de segurança deles no /tmp, expurgá-los, ignorar "
"esse problema completamente ou cancelar essa instalação e manualmente "
"manusear os arquivos salvos do Slash'em."

#. Type: select
#. Description
#. Type: select
#. Description
#: ../slashem-common.templates:1002 ../slashem-common.templates:2002
msgid ""
"If you choose to back up, the files will be backed up into a gzip-compressed "
"tar archive in /tmp with a random name starting with 'slash' and ending in '."
"tar.gz'."
msgstr ""
"Se você optar pela cópia de segurança, os arquivos serão salvos em um "
"arquivo tar compactado com gzip no /tmp com um nome aleatório começando com "
"'slash' e terminando com '.tar.gz'."
